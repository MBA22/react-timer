var TimerComponent = React.createClass({

    render: function () {

        if (this.props.timeLeft == null || this.props.timeLeft == 0)

            return <h2>Times up</h2>

        return <h3>Time left: {this.props.timeLeft}</h3>

    },

});


var InputButtonComponent = React.createClass({

    clickHandel: function () {
        var time = this.refs.timeInSec.value;
        if (time == 0 || time == null)
            time = 1;
        this.props.startTimer(time);
    },
    startTimer: function (e) {

        return this.props.startTimer(this.props.time)

    },

    render: function () {

        return (
            <div>
                <input ref="timeInSec" placeholder="time in Seconds" type="number" min='1'/>
                <button
                    type="button" className='btn btn-success' onClick={this.clickHandel}>Set Timer
                </button>
            </div>
        );

    },

});


var TimerMain = React.createClass({

    getInitialState() {
        return ({
            timeLeft: null,
            timer: null,
        });
    },
    startTimer: function (time) {
        var timeLeft = time;
        clearInterval(this.state.timer);

        var timer = setInterval(function () {

            console.log('2: Inside of setInterval');

            timeLeft = timeLeft - 1;

            if (timeLeft == 0) clearInterval(timer);

            this.setState({timeLeft: timeLeft});

        }.bind(this), 1000);

        console.log('1: After setInterval');

        return this.setState({timeLeft: timeLeft, timer: timer})

    },


    render: function () {
        return (

            <div className="row-fluid">
                <h2>Timer</h2>
                <div className="btn-group">
                    <InputButtonComponent startTimer={this.startTimer}/>
                </div>
                <TimerComponent timeLeft={this.state.timeLeft}/>
            </div>

        );
    }
});


var Main = React.createClass({
    render: function () {
        return (
            <div className="container">
                <h1>Timer</h1>
                <TimerMain className="text-center"/>
            </div>

        );
    },
});
ReactDOM.render(<TimerMain timeLeft={5}></TimerMain>, document.getElementById('app'));